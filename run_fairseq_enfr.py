import os
import sys
import torch

# Usage: python run-fairseq-enfr.py NOM_FICHIER_SOURCE

en2fr = torch.hub.load('pytorch/fairseq', 'transformer.wmt14.en-fr', tokenizer='moses', bpe='subword_nmt')

infi = sys.argv[1]
oufi = os.path.splitext(infi)[0] + '_out.txt'


with open(infi, mode='r', encoding='utf8') as ifh, open(oufi, mode='w', encoding='utf8') as ofh:
    outlines = []
    for line in ifh:
        print(f'{line.strip()}')
        out = en2fr.translate(line)
        #outlines.append(out.strip())
        print(f'{out.strip()}')
        ofh.write(f'{out.strip()}\n')
        ofh.flush()
        print("\n")
    #ofh.write("\n".join(outlines))
