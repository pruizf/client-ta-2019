#coding=utf8

"""Translates a sentence en-fr"""

import os

# Imports the Google Cloud client library
from google.cloud import translate_v2 as translate

# IDs d'accès
key = "" # donner chemin vers clé JSON créée avec Google Cloud Platform
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key

# Instantiates a client
translate_client = translate.Client()

# The text to translate
text = u'The report also said that although three-quarters of Chinese say their country is playing a larger role in the world than it did a decade ago, most want their government to focus on domestic issues rather than helping other nations.'
# The target language
target = 'fr'

# Translates some text into French
translation_b = translate_client.translate(text,
                target_language=target,
                model='base')

translation_n = translate_client.translate(text,
                target_language=target,
                model='nmt')

print('Text: {}'.format(text))
print('Translation B: {}'.format(translation_b['translatedText'].replace("&#39;", "")))
print('Translation N: {}'.format(translation_n['translatedText'].replace("&#39;", "")))
