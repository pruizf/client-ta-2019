# Cours TA : Clients pour outils pré-entraînés

## Fairseq

- Voir le script `run-fairseq-enfr.py`

## API de traduction Google

- Installer d'abord la librairie Python

```shell
pip install google-cloud-translate 
 ```

- Pour traduire un fichier (`translate_folder.py`) : 
  - Donner son emplacement :
      - Dossier : dans la variable `idir`
      - Fichier : dans la variale `infn`
  - Donner un dossier et fichier de sortie :
      - Dossier : dans `odir`
      - Fichier : dans `ofn`
  - Donner dans `key` le chemin vers la clé créée avec Google Cloud Platform
  - Exécuter (`python path/vers/le/script/translate_folder.py`)

- Pour traduire une chaîne de caractères (`sample-enfr.py`)
  - Donner le chemin vers la clé, dans `key`
  - Entrer la chaîne, dans `text`
  - Exécuter le script

