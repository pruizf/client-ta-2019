#coding=utf8

"""
Traduction avec modèles statistiques et neuronaux
disponibles avec API de Google
"""

import os

# demande pip install google-cloud-translate
from google.cloud import translate_v2 as translate

# IO
#   dossier du corpus
idir = "/home/ruizfabo/te/stra/cours/cours/ta/_2019/projet/wk/source_small"
#   nom du fichier du corpus
infn = os.path.join(idir, "collected-sentences.txt")
#   dossier de sortie (pour les traductions)
odir = "/home/ruizfabo/te/stra/cours/cours/ta/_2019/projet/wk/source_small_out"
#   nom du fichier de sortie
ofn = os.path.join(odir, "collected-sentences_out.txt")
if not os.path.exists(odir):
    os.makedirs(odir)

# Params
#    donner chemin vers la clé JSON créée avec Google Cloud Platform
key = "/path/vers/cle/xxxx.json"
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = key
target = 'fr'

# instantier client pour l'API
translate_client = translate.Client()

# traduire
with open(infn) as ifh, open(ofn, "w") as ofh:
    sents = [ll.strip() for ll in ifh]
    for sent in sents:
        print(sent)
        # trad PBMT
        translation_b = translate_client.translate(sent,
                        target_language=target,
                        model='base')
        # trad NMT
        translation_n = translate_client.translate(sent,
                        target_language=target,
                        model='nmt')
        # infos de sortie
        outinfos = (sent,
                    translation_b['translatedText'].replace("&#39;", "'"),
                    translation_n['translatedText'].replace("&#39;", "'"))
        print(outinfos)
        ofh.write("".join(("\t".join(outinfos), "\n")))
        ofh.flush()
